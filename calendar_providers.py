#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scribus
import collections
import datetime
from datetime import timedelta

from dateutil.easter import easter

from name_dates import DATE_NAMES


TITLE_DATE_FORMAT = "%B"


class CalendarProvider(collections.Iterator):
    def next(self):
        pass


def month_calendar_provider(year, part=1):
    for i in range(12 / part):
        for month in range(i, 12, 12 / part):
            yield MonthGroup(year, month + 1)


class MonthCalendarProvider(CalendarProvider):
    def __init__(self, year, part=1):
        self.year = year
        self.month = 0
        self.part = part

    def next(self):
        self.month += 1
        if self.month > 12:
            raise StopIteration

        return MonthGroup(self.year, self.month)


class WeekCalendarProvider(CalendarProvider):
    def __init__(self, year):
        self.year = year
        self.current_day = datetime.date(year, 1, 1)
        self.week_number = -1

        # Make current_day Monday
        while self.current_day.weekday() != 0:
            self.current_day -= datetime.timedelta(days=1)

    def next(self):
        if self.current_day.year > self.year:
            raise StopIteration
        old = self.current_day

        self.current_day += datetime.timedelta(days=7)
        self.week_number += 1
        return WeekGroup(old, self.week_number)


class MonthGroup(collections.Iterator):
    def __init__(self, year, month):
        self.current_date = datetime.date(year, month, 1)
        self.month = month

        self._number_of_days = 0
        last_day_of_month = datetime.date(year, month, 1)
        while last_day_of_month.month == self.month:
            last_day_of_month += datetime.timedelta(days=1)
            self._number_of_days += 1

    def count(self):
        return self._number_of_days

    def next(self):
        if self.current_date.month != self.month:
            raise StopIteration
        old = self.current_date
        self.current_date += datetime.timedelta(days=1)
        return old

    def title(self):
        return str(self.current_date.strftime(TITLE_DATE_FORMAT))

    @property
    def index(self):
        return self.month - 1


class WeekGroup(collections.Iterator):
    def __init__(self, start_date, week_number):
        self.current_date = start_date
        self.max_date = start_date + datetime.timedelta(days=6)
        self._week_number = week_number

    def count(self):
        return 7

    def next(self):
        if self.current_date > self.max_date:
            raise StopIteration
        old = self.current_date
        self.current_date += datetime.timedelta(days=1)

        return old

    def title(self):
        return str(self._week_number)


class EasterProvider:
    def get_name(self, date):
        pass


class CzechEasterProvider(EasterProvider):
    def __init__(self, year):
        self.easter_date = easter(year)

        self.date_names = {
            self.easter_date + timedelta(days=-46): "Popeleční středa",
            self.easter_date + timedelta(days=-7): "Květná neděle",
            self.easter_date + timedelta(days=-3): "Zelený čtvrtek",
            self.easter_date + timedelta(days=-2): "Velký pátek",
            self.easter_date + timedelta(days=-1): "Bílá sobota",
            self.easter_date: "Zmrtvýchvstání Páně",
            self.easter_date + timedelta(days=39): "Svátek Nanebevstoupení Páně",
            self.easter_date + timedelta(days=49): "Seslání Ducha svatého",
        }

    def get_name(self, date):
        return self.date_names.get(date)


class DateNameProvider:
    def date_name(self, date):
        pass


class CzechDateNameProvider(DateNameProvider):
    def __init__(self, year):
        self.easter_names = CzechEasterProvider(year)

    def get_name(self, date):
        texts = DATE_NAMES[date.month][date.day].split("|")
        easter_name = self.easter_names.get_name(date)
        if easter_name:
            texts.append(easter_name)
        return texts
