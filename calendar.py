#!/usr/bin/env python
# -*- coding: utf-8 -*-


import copy
import datetime
import glob
import logging
import os

import scribus

from calendar_providers import (
    WeekCalendarProvider,
    CzechDateNameProvider,
    month_calendar_provider,
)

CONSTANTS_ENABLED = False
# If False, all setting is gotten from dialogs
# and constants is not used.

# Page settings
PAGE_SIZE = (210, 297)
PAGE_MARGINS = (7, 7, 7, 7)
PAGE_ORIENTATION = scribus.PORTRAIT
PAGE_UNIT = scribus.UNIT_MILLIMETERS

GROUPS_ON_PAGE = 2
DATE_WIDTH = 50  # in %
USER_COL = [" "]  # ['maminka', 'P+B', 'F+D', 'zvířena']
DATE_FORMAT = "%a %-d. "  # following by name from DateNameProvider

STATIC_HORIZONTAL_SPACE = None

IMAGE_FOLDER = "/home/lachman/Pictures/"

# TOP_MARGIN
# SPACE_0 = 0
# TITLE_HEIGHT = 10
SPACE_0 = 0
# PICTURE
SPACE_1 = 5
HEADLINE_HEIGHT = 10
SPACE_2 = 5
# TABLE
SPACE_3 = 0

# BOTTOM_MARGIN

###############

# internal constants;:

CPS_TITLE = "cps Title"
CPS_COLUMN_HEADLINE = "cps Column Headline"
CPS_DATE = "cps Date"
CPS_DATE_NAME = "cps Date Name"
CPS_DATE_WEEKEND = "cps Date (weekend)"
CPS_DATE_NAME_WEEKEND = "cps Date Name (weekend)"
CPS_TABLE = "cps Table"

FRIDAY = 4

DUPLICABLE_FRAME = None


class CalendarDraw:
    """
    Class for generating calendars.
    """

    def __init__(
        self,
        date_name_provider,
        calendar_provider,
        headlines,
        date_format,
        image_folder,
        groups_on_page,
        static_horizontal_space,
        count=1,
    ):
        """
        Creates generator for calendars.
        :param date_name_provider: Object providing names for each date in year.
        :param calendar_provider: Object providing dates by groups for one year.
        :param headlines: User columns' headlines
        :param date_format: Python date format for date
        :param image_folder: Folder with images to use.
        :param groups_on_page: Number of groups on one page.
        :param static_horizontal_space: If it is None or '' , then the space will be calculated dynamically.
        """

        self.name_date = date_name_provider
        self.calendar = multipler(calendar_provider, count)
        self.headlines = headlines
        self.date_format = date_format
        self.image_folder = image_folder

        self.images = self.find_images()

        self._page_status = PageStatus(
            scribus.getPageSize(),
            scribus.getPageMargins(),
            groups_on_page,
            len(self.headlines),
            DATE_WIDTH,
            HEADLINE_HEIGHT,
            [SPACE_0, SPACE_1, SPACE_2, SPACE_3],
            static_horizontal_space,
        )

        self._create_styles()

        self.status = 0
        scribus.progressReset()
        scribus.progressTotal(100)
        scribus.progressSet(self.status)

        logging.debug("preparation finished")

    def _create_and_select_new_page(self):
        logging.debug("new page, select new page")
        scribus.newPage(-1)

    def draw(self):
        """
        Generates a calendar in open document.
        """

        if not scribus.haveDoc():
            scribus.messageBox(
                "Script failed",
                "This script only works when you have set a document settings.",
                scribus.ICON_ERROR,
            )
            return

        logging.debug("start drawing")
        scribus.redrawAll()

        for group in self.calendar:
            scribus.statusMessage("new group - " + group.title())
            logging.debug("new group - " + group.title())

            self._page_status.next_group(group.count())

            if self._page_status.is_new_page:
                self._create_and_select_new_page()

            self._page_status.add_space(0)
            self._draw_picture(index=group.index)
            self._page_status.add_space(1)
            self._draw_table_headline(group.title())
            self._page_status.add_space(2)
            for day in group:
                self._draw_day(day)
            self._page_status.add_space(3)

        scribus.progressReset()

    def _draw_picture(self, index):
        left = self._page_status.left
        top = self._page_status.top
        width = self._page_status.group_width
        height = self._page_status.group_width

        i_path = self.images[index]
        logging.debug("Adding image : {}".format(i_path))
        i = scribus.createImage(left, top, width, height)
        scribus.loadImage(i_path, i)

        self._fit_image(i)

        self._page_status.add_top(height)

    def _fit_image(self, image_frame):
        """
        Fits image to frame.
        :param image_frame: Frame image to fit image in.
        """
        restore_units = scribus.getUnit()
        scribus.setUnit(0)

        scribus.setScaleImageToFrame(1, 1, image_frame)
        frameW, frameH = scribus.getSize(image_frame)
        saveScaleX, saveScaleY = scribus.getImageScale(image_frame)
        scribus.setScaleImageToFrame(1, 0, image_frame)
        fullScaleX, fullScaleY = scribus.getImageScale(image_frame)
        scribus.setScaleImageToFrame(0, 0, image_frame)
        scribus.setImageScale(saveScaleX, saveScaleY, image_frame)
        imageW = frameW * (saveScaleX / fullScaleX)
        imageH = frameH * (saveScaleY / fullScaleY)
        imageY = (frameH - imageH) / 2.0
        imageX = (frameW - imageW) / 2.0
        scribus.setImageOffset(imageX, imageY, image_frame)
        scribus.setUnit(restore_units)

    def _draw_table_headline(self, title):
        """
        Draw table headline consisting of group title and user column headlines
        :param title: title of group
        """
        left = self._page_status.left
        top = self._page_status.top
        width = self._page_status.date_width
        height = self._page_status.headline_height
        logging.debug("new headlines with title: {}".format(str(title)))

        # title
        text_headline = self._get_text_frame(left, top, width, height)
        scribus.setText(str(title), text_headline)
        self._style_title(
            text_headline, vertical_shift=2 * self._page_status.date_height()
        )

        self._page_status.add_left_in_group(width)

        # user column headlines
        user_column_width = self._page_status.user_column_width
        for h in self.headlines:
            text_column_header = self._get_text_frame(
                self._page_status.left, self._page_status.top, user_column_width, height
            )
            scribus.setText(h, text_column_header)
            self._style_headline(text_column_header)

            self._page_status.add_left_in_group(user_column_width)

        self._page_status.add_top(height)
        self._page_status.clear_left_in_group()

    def _draw_day(self, date):
        """
        Draw one day - one line in table consisting of formatted date, name of date and free cell for each user column.
        :param date: Date to generate.
        """
        left = self._page_status.left
        top = self._page_status.top
        width = self._page_status.date_width
        height = self._page_status.date_height()
        is_weekend = date.weekday() > FRIDAY

        # Name part
        texts = self.name_date.get_name(date)

        logging.debug(
            'new date {} with text: "{}"'.format(
                str(date),
                date.strftime(self.date_format) + str(texts),
                str(left),
                str(top),
                str(width),
                str(height),
            )
        )
        text_date_frame = self._get_text_frame(left, top, width, height)
        self._style_border(text_date_frame)

        # Date part
        text_date_height = 2 * height
        text_date = self._get_text_frame(left, top, text_date_height, height)
        scribus.setText(date.strftime(self.date_format), text_date)
        self._style_date(text_date, is_weekend)

        space = height / 10.0
        line_height = (height - 2 * space) / len(texts)
        for t in texts:
            text_date_name = self._get_text_frame(
                left + text_date_height,
                top + space,
                width - text_date_height,
                line_height,
            )
            scribus.setText(t, text_date_name)
            self._style_date_name(text_date_name, is_weekend)
            space += line_height

        self._page_status.add_left_in_group(width)

        user_column_width = self._page_status.user_column_width
        for h in self.headlines:
            text_table_cell = self._get_text_frame(
                self._page_status.left, self._page_status.top, user_column_width, height
            )
            scribus.setText(" ", text_table_cell)
            self._style_table_text(text_table_cell)

            self._page_status.add_left_in_group(user_column_width)

        self._page_status.add_top(height)
        self._page_status.clear_left_in_group()
        self._update_status()

    def _update_status(self):
        self.status += 1
        progress = 100 * self.status // 366
        logging.debug("Progress: {}%".format(progress))
        # scribus.progressSet(progress)

    def _style_title(self, text_frame, vertical_shift=0):
        """
        Style title of group.
        :param text_frame: frame with group title
        :return:
        """
        logging.debug("Vertical_shift: {}".format(vertical_shift))
        self._style_text_frame(text_frame, vertical_shift=vertical_shift)
        self._style_border(text_frame)
        scribus.setStyle(CPS_TITLE, text_frame)

    def _style_headline(self, text_frame):
        """
        Style user columns' headlines
        :param text_frame: frame with user column headline
        :return:
        """
        scribus.setStyle(CPS_COLUMN_HEADLINE, text_frame)
        self._style_text_frame(text_frame, vertical_shift=0)
        self._style_border(text_frame)

    def _style_date(self, text_frame, is_weekend=False):
        """
        Style cell with formatted date. If it is weekend, we will use another style.
        :param text_frame: text frame with date
        :param is_weekend: whether is weekend or not.
        """
        self._style_text_frame(text_frame)
        if is_weekend:
            scribus.setStyle(CPS_DATE_WEEKEND, text_frame)
        else:
            scribus.setStyle(CPS_DATE, text_frame)

    def _style_date_name(self, text_frame, is_weekend=False):
        """
        Style text frame wiht name of day. If it is weekend, we will use another style.
        :param text_frame: text frame to format
        :param is_weekend: whether it is weekend
        """
        self._style_text_frame(text_frame, vertical_shift=0)
        if is_weekend:
            scribus.setStyle(CPS_DATE_NAME_WEEKEND, text_frame)
        else:
            scribus.setStyle(CPS_DATE_NAME, text_frame)

    def _style_table_text(self, text_frame):
        """
        Style basic cell in table.
        :param text_frame: Text frame to format.
        """
        self._style_text_frame(text_frame)
        self._style_border(text_frame)
        scribus.setStyle(CPS_TABLE, text_frame)

    def _style_text_frame(self, text_frame, vertical_shift=0.0):
        """
        Basic formating of text frame - set top distance.
        :param text_frame: Text frame to style.
        :param vertical_shift: Distance from top.
        """
        distances = scribus.getTextDistances(text_frame)
        # logging.debug("Distances: {}".format(distances))
        scribus.setTextDistances(
            vertical_shift, distances[1], distances[2], distances[3], text_frame
        )

    def _style_border(self, text_frame):
        """
        Basic formating of text_frame border.
        :param text_frame: Frame to format.
        """
        scribus.setLineColor("Black", text_frame)
        scribus.setLineWidth(1, text_frame)

    def vertical_align(self, text_frame):
        """
        Thanks to Gregory Pittman for making centervert.py.
        """
        restore_units = (
            scribus.getUnit()
        )  # since there is an issue with units other than points,
        scribus.setUnit(0)

        lines = scribus.getTextLines(text_frame)
        distances = scribus.getTextDistances(text_frame)
        linespace = scribus.getFontSize(text_frame)
        dimensions = scribus.getSize(text_frame)  # (width, height)

        if scribus.textOverflows(text_frame, 1) > 0:
            logging.error("This frame is already overflowing")
            return
        newtopdist = (dimensions[1] - linespace * lines) / 2
        scribus.setTextDistances(
            distances[0], distances[1], newtopdist, distances[3], text_frame
        )

        scribus.setUnit(restore_units)

    def find_images(self):
        image_list = []
        for filename in glob.glob(self.image_folder + "/*"):
            image_list.append(filename)
            logging.debug("find image : " + filename)

        log_message = "number of images : {}".format(str(len(image_list)))
        logging.debug(log_message)

        image_list.sort()

        return image_list

    def _create_styles(self):

        logging.debug("create paragraph styles")

        scribus.createParagraphStyle(CPS_DATE, linespacingmode=1, alignment=2)
        scribus.createParagraphStyle(CPS_DATE_WEEKEND, linespacingmode=1, alignment=2)
        scribus.createParagraphStyle(CPS_TABLE, linespacingmode=1)
        scribus.createParagraphStyle(
            CPS_COLUMN_HEADLINE, linespacingmode=1, alignment=1
        )
        scribus.createParagraphStyle(CPS_DATE_NAME, linespacingmode=1, alignment=0)
        scribus.createParagraphStyle(
            CPS_DATE_NAME_WEEKEND, linespacingmode=1, alignment=0
        )
        scribus.createParagraphStyle(CPS_TITLE, linespacingmode=1, alignment=0)

    def _get_text_frame(self, x, y, width, height):
        scribus.pasteObject()  # scribus.createText(left, top, text_date_height, height)
        text_frame = scribus.getSelectedObject()
        scribus.moveObjectAbs(x, y, text_frame)
        scribus.sizeObject(width, height, text_frame)
        return text_frame


class PageStatus:
    def __init__(
        self,
        page_size,
        margins,
        groups_on_page,
        user_column_number,
        date_width,
        headline_height,
        spaces,
        horizontal_space,
    ):

        page_width, page_height = page_size
        logging.debug(
            "page format : width={}, height={}".format(
                str(page_width), str(page_height)
            )
        )

        self.top_margin, self.left_margin, self.right_margin, self.bottom_margin = (
            margins
        )

        self.page_width = page_width - self.right_margin - self.left_margin
        self.page_height = page_height - self.top_margin - self.bottom_margin
        logging.debug(
            "printable space format : page_width={}, page_height={}".format(
                str(self.page_width), str(self.page_height)
            )
        )

        self.left = self.left_margin
        self._group_left = self.left_margin
        self.top = self.top_margin
        logging.debug("starting pointer : {}x{}".format(str(self.left), str(self.top)))

        if horizontal_space is None or horizontal_space == "":
            self.horizontal_space = self.left_margin + self.right_margin
        else:
            self.horizontal_space = float(horizontal_space)

        self.group_width = (
            self.page_width - (groups_on_page - 1) * self.horizontal_space
        ) / groups_on_page
        logging.debug("group_width={}".format(str(self.group_width)))

        if user_column_number > 0:
            self.user_column_width = (
                self.group_width - date_width * self.group_width / 100
            ) / user_column_number
            self.date_width = date_width * self.group_width / 100
        else:
            self.user_column_width = 0
            self.date_width = self.group_width

        self._group_number_on_page = 0
        self.groups_on_page = groups_on_page

        self.days_in_group = 0
        self.is_new_page = False

        self.spaces = spaces
        self.headline_height = headline_height

        sum_of_spaces = 0
        for s in spaces:
            sum_of_spaces += s

        self.space_for_table = (
            self.page_height - self.group_width - sum_of_spaces - headline_height
        )

    def add_top(self, shift):
        self.top += shift

    def add_left_in_group(self, shift):
        self.left += shift

    def add_left(self, shift):
        self._group_left += shift
        self.left += shift

    def clear_left_in_group(self):
        self.left = self._group_left

    def next_group(self, days_in_group):
        self.days_in_group = days_in_group
        self.top = self.top_margin

        if self._group_number_on_page >= self.groups_on_page:
            self.left = self.left_margin
            self._group_left = self.left_margin
            self._group_number_on_page = 0
            self.is_new_page = True
        elif self._group_number_on_page > 0:
            shift = self.group_width + self.horizontal_space
            self.left += shift
            self._group_left += shift
            self.is_new_page = False

        self._group_number_on_page += 1

    def add_space(self, space_number):
        self.top += self.spaces[space_number]

    def date_height(self):
        return self.space_for_table / self.days_in_group


def multipler(some_iterator, n):
    for item in some_iterator:
        for _ in range(n):
            yield copy.copy(item)


###################

logging.basicConfig(
    filename=os.path.expanduser("~/.scribus_calendar.log"),
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s : %(message)s",
)

logging.info(
    "-------------------- START FAMILY CALENDAR GENERATOR FOR SCRIBUS --------------------"
)

try:
    year_raw = scribus.valueDialog(
        "Year", "Choose the year:", str(datetime.datetime.today().year + 1)
    )
    logging.debug("groups on page: {}".format(year_raw))
    year = int(year_raw)

    count_raw = scribus.valueDialog("Count", "Number of calendars:", "1")
    logging.debug("count: {}".format(count_raw))
    count = int(count_raw)

    calendar_type = scribus.valueDialog(
        "Calendar type", "Choose calendar type:\n`month` or `week`", "month"
    ).lower()

    DUPLICABLE_FRAME = scribus.getSelectedObject()
    scribus.copyObject(DUPLICABLE_FRAME)

    group = scribus.valueDialog(
        "Groups", "Choose number of parts:\ne.g. 2 for 1,6,2,7,...", "1"
    )
    try:
        group_number = int(group)
    except:
        raise Exception("Cannot get number of groups from '{}'.".format(group))

    if calendar_type == "month":
        provider = month_calendar_provider(year, part=group_number)
    elif calendar_type == "week":
        provider = WeekCalendarProvider(year)
    else:
        raise Exception("No know calendar type.")

    if CONSTANTS_ENABLED:
        logging.info("constants enabled")
        if not scribus.newDocument(
            PAGE_SIZE,
            PAGE_MARGINS,
            PAGE_ORIENTATION,
            0,
            PAGE_UNIT,
            scribus.PAGE_1,
            0,
            1,
        ):
            raise Exception("Cannot create document.")
        headlines = USER_COL
        date_format = DATE_FORMAT
        image_folder = IMAGE_FOLDER
        static_horizontal_space = STATIC_HORIZONTAL_SPACE
        groups_on_page = GROUPS_ON_PAGE
    else:
        logging.info("settings from dialogs")

        # user columns
        try:
            headlines_raw = scribus.valueDialog(
                "Columns", "Define user columns with their name divided by `;` sign."
            )
            if headlines_raw == "":
                headlines = []
            else:
                headlines = headlines_raw.split(";")
            logging.debug("user columns : {}".format(str(headlines)))
        except:
            raise Exception("Columns not recognised.")

        date_format = scribus.valueDialog(
            "Date format", "Define formatting of date", DATE_FORMAT
        )
        logging.debug("date format : {}".format(date_format))

        image_folder = scribus.fileDialog("Choose directory with images", isdir=True)

        static_horizontal_space = scribus.valueDialog(
            "Static horizontal space",
            "Determine static horizontal space, else it will be calculated dynamically.",
        )
        logging.debug("static horizontal space: {}".format(static_horizontal_space))

        groups_on_page_raw = scribus.valueDialog(
            "Groups on page", "Choose number of groups on page:", "2"
        )
        logging.debug("groups on page: {}".format(groups_on_page_raw))
        groups_on_page = int(groups_on_page_raw)

        scribus.newDocDialog()

    c = CalendarDraw(
        date_name_provider=CzechDateNameProvider(year),
        calendar_provider=provider,
        headlines=headlines,
        date_format=date_format,
        image_folder=image_folder,
        static_horizontal_space=static_horizontal_space,
        groups_on_page=groups_on_page,
        count=count,
    )
    c.draw()

except Exception as ex:
    logging.error(ex)
    scribus.messageBox(caption="Script failed", message=ex.message)

logging.info(
    "-------------------- STOP FAMILY CALENDAR GENERATOR FOR SCRIBUS --------------------"
)

"""
pdf_path = './calendar.pdf'
pdf = scribus.PDFfile()
pdf.file = pdf_path
pdf.save()
"""
###############x

# TODO:
# width of date cell has to be smaller then width of space for date+datename (problem with week calendar)
